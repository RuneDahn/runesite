'use strict'
let fs = require('fs');
let path = require('path');
let express = require('express');
let app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

let publicFolder = path.join(__dirname, 'public');
app.use('/images', express.static(path.join(publicFolder, 'images')));
app.use('/src', express.static(path.join(publicFolder, 'node_modules')));
app.use('/js', express.static(path.join(publicFolder, 'js')));

fs.readdirSync('./controllers').forEach(file => {
    if(file.substr(-3) == '.js') {
        let controller = require('./controllers/' + file);
        controller(app, express);
    }
});

app.listen(3000, () => {
    let dt = new Date();
    console.log(`Server iniciado - ${dt.toLocaleTimeString()}`);
})
