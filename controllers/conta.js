'use strict';
module.exports = (app, express) => {
    let router = express.Router();
    app.use('/usuario', router);

    router.get('/', (req, res)=> {
        res.render('main.pug', {
            title: 'Olá!',
            msg: 'Bem vindo, anonimo!'
        });
    });

    router.get('/:nome', (req, res) => {
        res.render('main.pug', {
            title: 'Olá!',
            msg: `Bem vindo, ${req.params.nome}!`
        });
    });

}