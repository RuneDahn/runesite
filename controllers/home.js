'use strict';
module.exports = (app, express) => {
    let router = express.Router();
    app.use('/', router);

    router.get('/', (req, res) => {
        res.render('main.pug', {
            title: 'It works?',
            msg: 'Maybe...'
        });
    });

}